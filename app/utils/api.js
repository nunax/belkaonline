import * as http from "tns-core-modules/http";

export const Auth = {
  async Login(user, password){

    try {
      console.log(http);
      const response = await http.request({
        url: "http://game11.herokuapp.com/api/auth/login/",
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        content: JSON.stringify({
          email: user,
          password: password
        })
      })
      console.log(`RESPONCE CODE => ${response}`)
      console.log(`RESPONCE CONTENT => ${response.content}`)
      
      this.SetUserData(JSON.parse(response.content))

      return true;

    } catch (error) {
      console.error(`ERROR POST REQUEST 123 => ${error}`)

      return false;
    }
  }

}
 
export const Profile = {
  async GetUserProfile() {
    try {
      console.log(http);
      const response = await http.request({
        url: `http://game11.herokuapp.com/api/profile/`,
        method: "GET",
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.token
        },
      })
      console.log(`RESPONCE CODE => ${response}`)
      console.log(`RESPONCE CONTENT => ${response.content}`)

      this.SetUserProfile(JSON.parse(response.content))

      return true;
    } catch (error) {
      console.error(`ERROR POST REQUEST => ${error}`)

      return false;
    }
  }
}