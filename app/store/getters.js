
function LOG(message){
  console.log(`#### GETTERS -> ${message}`)
}

export const GET_TOKEN = state => {
  LOG(`GET TOKEN => ${state.userData.token}`)
  return state.userData.token
}