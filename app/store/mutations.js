
function LOG(message){
  console.log(`#### MUTATION -> ${message}`)
}

export const SET_USER_DATA = (state, value) => {
  LOG(`SET_USER_DATA => value: ${value}`)
  state.userData = value;
}

export const SET_USER_PROFILE = (state, value) => {
  LOG(`SET_USER_PROFILE => value: ${value}`)
  state.userProfile = value;
}