// https://github.com/nativescript-vue/nativescript-vue-navigator

import StartPage from './components/Home'

export const routes = {
  '/start': {
    component: StartPage,
  },
  /*'/home': {
    component: HomePage,
  },
  '/gestureparameterpage': {
    component: GestureParameterPage,
  },
  '/gestureconfigpage': {
    component: GestureConfigPage,
  }
  /*'/quicksettingspage': {

  },*/
}